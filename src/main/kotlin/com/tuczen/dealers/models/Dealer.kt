package com.tuczen.dealers.models

import java.util.*
import javax.persistence.*

@Entity
class Dealer {

    @Id
    @GeneratedValue
    @Column(nullable = false, unique = true, columnDefinition = "BINARY(16)")
    var id: UUID? = null

    @Column(nullable = false)
    var name: String? = null
}