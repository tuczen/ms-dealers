package com.tuczen.dealers

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DealersApplication

fun main(args: Array<String>) {
	runApplication<DealersApplication>(*args)
}
