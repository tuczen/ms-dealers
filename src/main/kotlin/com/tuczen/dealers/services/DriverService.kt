package com.tuczen.dealers.services

import com.tuczen.dealers.exceptions.ServiceException
import com.tuczen.dealers.models.Driver
import com.tuczen.dealers.repositories.DriverRepository
import javassist.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import kotlin.jvm.Throws

@Service
class DriverService: IDriverService {

    @Autowired
    val driverRepository: DriverRepository? = null

    @Throws(ServiceException::class)
    override fun findAll(): List<Driver> {
        try {
            return driverRepository!!.findAll()
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }
    }

    @Throws(ServiceException::class)
    override fun save(entity: Driver): Driver {
        try {
            return driverRepository!!.save(entity)
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }
    }

    @Throws(ServiceException::class, NotFoundException::class)
    override fun destroy(id: UUID, dealerId: UUID) {
        findOrFail(id)

        try {
            driverRepository!!.deleteByIdAndDealerId(id, dealerId)
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }
    }

    fun findOrFail(id: UUID): Driver {
        val op: Optional<Driver>

        try {
            op = driverRepository!!.findById(id)
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }

        if (!op.isPresent) {
            throw NotFoundException("Model not found")
        }

        return op.get()
    }
}