package com.tuczen.dealers.exceptions

class ServiceException(message:String?): Exception(message)