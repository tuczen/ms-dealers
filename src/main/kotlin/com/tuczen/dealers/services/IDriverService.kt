package com.tuczen.dealers.services

import com.tuczen.dealers.models.Driver
import java.util.*

interface IDriverService {
    fun findAll(): List<Driver>

    fun save(entity: Driver): Driver

    fun destroy(id: UUID, dealerId: UUID)
}