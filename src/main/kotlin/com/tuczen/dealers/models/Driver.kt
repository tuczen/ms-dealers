package com.tuczen.dealers.models

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class Driver {

    @Id
    @Column(nullable = false, columnDefinition = "BINARY(16)")
    var id: UUID? = null

    @Column(nullable = false, columnDefinition = "BINARY(16)")
    var dealerId: UUID? = null
}