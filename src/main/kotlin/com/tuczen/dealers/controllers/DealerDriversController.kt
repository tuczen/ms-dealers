package com.tuczen.dealers.controllers;

import com.tuczen.dealers.models.Driver
import com.tuczen.dealers.services.IDriverService
import javassist.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("api/v1/dealers/{dealerId}/drivers")
class DealerDriversController {

    @Autowired
    val driverService: IDriverService? = null

    @GetMapping
    fun index(@PathVariable("dealerId") dealerId: UUID): ResponseEntity<List<Driver>> {
        return ResponseEntity(driverService!!.findAll(), HttpStatus.OK)
    }

    @PostMapping
    fun create(@PathVariable("dealerId") dealerId: UUID, @RequestBody driver: Driver): ResponseEntity<Any> {
        driver.dealerId = dealerId
        driverService!!.save(driver)
        return ResponseEntity(HttpStatus.CREATED)
    }

    @DeleteMapping("{id}")
    fun destroy(@PathVariable("dealerId") dealerId: UUID, @PathVariable("id") id: UUID): ResponseEntity<Any> {
        return try {
            driverService!!.destroy(id, dealerId)
            ResponseEntity(HttpStatus.OK)
        } catch (e: NotFoundException) {
            ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
