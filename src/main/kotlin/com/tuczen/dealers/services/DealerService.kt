package com.tuczen.dealers.services

import com.tuczen.dealers.exceptions.ServiceException
import com.tuczen.dealers.models.Dealer
import com.tuczen.dealers.repositories.DealerRepository
import javassist.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import kotlin.jvm.Throws

@Service
class DealerService: IDealerService {

    @Autowired
    val dealerRepository: DealerRepository? = null

    @Throws(ServiceException::class)
    override fun findAll(): List<Dealer> {
        try {
            return dealerRepository!!.findAll()
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }
    }

    @Throws(ServiceException::class)
    override fun save(entity: Dealer): Dealer {
        try {
            return dealerRepository!!.save(entity)
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }
    }

    @Throws(ServiceException::class, NotFoundException::class)
    override fun find(id: UUID): Dealer {
        return findOrFail(id)
    }

    @Throws(ServiceException::class, NotFoundException::class)
    override fun update(id: UUID, entity: Dealer): Dealer {
        findOrFail(id)

        try {
            return dealerRepository!!.save(entity)
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }
    }

    @Throws(ServiceException::class, NotFoundException::class)
    override fun destroy(id: UUID) {
        findOrFail(id)

        try {
            dealerRepository!!.deleteById(id);
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }
    }

    fun findOrFail(id: UUID): Dealer {
        val op: Optional<Dealer>

        try {
            op = dealerRepository!!.findById(id)
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }

        if (!op.isPresent) {
            throw NotFoundException("Model not found")
        }

        return op.get()
    }
}