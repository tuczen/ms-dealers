package com.tuczen.dealers.controllers

import com.tuczen.dealers.models.Dealer
import com.tuczen.dealers.services.IDealerService
import javassist.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.bind.annotation.PathVariable

import org.springframework.web.bind.annotation.GetMapping

import org.springframework.web.bind.annotation.RequestBody

import org.springframework.web.bind.annotation.PutMapping

import org.springframework.web.bind.annotation.DeleteMapping
import java.util.*


@RestController
@RequestMapping("api/v1/dealers")
class DealersController {

    @Autowired
    val dealerService: IDealerService? = null

    @GetMapping
    fun index(): ResponseEntity<List<Dealer>> {
        return ResponseEntity(dealerService!!.findAll(), HttpStatus.OK)
    }

    @PostMapping
    fun create(@RequestBody dealer: Dealer): ResponseEntity<Any> {
        dealerService!!.save(dealer)
        return ResponseEntity(HttpStatus.CREATED)
    }

    @DeleteMapping("{id}")
    fun destroy(@PathVariable("id") id: UUID): ResponseEntity<Any> {
        return try {
            dealerService!!.destroy(id)
            ResponseEntity(HttpStatus.OK)
        } catch (e: NotFoundException) {
            ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("{id}")
    fun update(@PathVariable id: UUID, @RequestBody dealer: Dealer): ResponseEntity<Any> {
        return try {
            dealerService!!.update(id, dealer)
            ResponseEntity(HttpStatus.OK)
        } catch (e: NotFoundException) {
            ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("{id}")
    fun show(@PathVariable("id") id: UUID): ResponseEntity<Any> {
        return try {
            ResponseEntity(dealerService!!.find(id), HttpStatus.OK)
        } catch (e: NotFoundException) {
            ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}