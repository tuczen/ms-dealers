package com.tuczen.dealers.services

import com.tuczen.dealers.models.Dealer
import java.util.*

interface IDealerService {
    fun findAll(): List<Dealer>

    fun save(entity: Dealer): Dealer

    fun find(id: UUID): Dealer

    fun update(id: UUID, entity: Dealer): Dealer

    fun destroy(id: UUID)
}