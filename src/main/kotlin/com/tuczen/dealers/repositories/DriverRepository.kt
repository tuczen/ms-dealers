package com.tuczen.dealers.repositories

import com.tuczen.dealers.models.Driver
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Repository
interface DriverRepository: JpaRepository<Driver, UUID> {

    @Transactional
    fun deleteByIdAndDealerId(id: UUID, dealerId: UUID)
}