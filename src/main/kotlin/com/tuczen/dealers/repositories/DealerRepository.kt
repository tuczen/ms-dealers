package com.tuczen.dealers.repositories

import com.tuczen.dealers.models.Dealer
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface DealerRepository: JpaRepository<Dealer, UUID> {
}